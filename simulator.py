# -*- coding: utf-8 -*-

from __future__ import annotations

__author__ = 'Yoshinori Kamijima'
__date__ = '2022/9/23'

from collections.abc import Iterator
import nptyping
import numpy as np
import numpy.typing as npt
from typing import cast, Optional, Union
import warnings

Node = Union[int, str]
Edge = tuple[Node, Node]
EdgeSet = set[Edge]

class BondPercolation:
    '''Bernoulli bond percolation

    Attributes
    ----------
    AdjacencyMatrix : NDArray[(Size, Size), int]
        The adjacency matrix of a given graph.
        Each component takes the value -1, 0 or +1.
        The value 0 means that there is no edge between the nodes indicated by indices.
        The values -1 and +1 mean that the bond is vacant and occupied, respectively.

    NumClusters : NDArray[Size, int]
        The cluster size distribution.
        Note that its index means size of a cluster.
        Each site is a cluster itself, hence NumClusters[0] is always 0.

    FirstClusterMoment : int
        The sum of the sizes of clusters weighted by the cluster size distribution over all sizes.

    SecondClusterMoment : int
        The sum of the squired sizes of clusters weighted by the cluster size distribution over all sizes.
    '''

    def __init__(self, edgeSet: EdgeSet) -> None:
        self.__rng: np.random.Generator = np.random.default_rng()
        self.__nodeIndices: dict[Node, int] = {  # Mapping from node labels to the indices of _parent.
            node: index
            for index, node in enumerate({n for pair in edgeSet for n in pair})
        }
        numNodes: int = len(self.__nodeIndices)
        self.__adjacencyMatrix: nptyping.NDArray[nptyping.Shape['Size, Size'], nptyping.Int] = np.zeros((numNodes,) * 2, dtype=np.int_)
        for edge in edgeSet:
            if self.__adjacencyMatrix[self.__nodeIndices[edge[0]]][self.__nodeIndices[edge[1]]] != 0:
                warnings.warn(f'The edge {edge} is duplicated.  Its bias gets overwritten by the successor.')
            self.__adjacencyMatrix[self.__nodeIndices[edge[0]], self.__nodeIndices[edge[1]]] = self.__adjacencyMatrix[self.__nodeIndices[edge[1]], self.__nodeIndices[edge[0]]] = -1
        self.__parent: npt.NDArray[np.int_] = np.full(numNodes, -1, dtype=np.int_)
        order: list[tuple[int, int]] = list(map(
            lambda nonZeroEdge: cast(tuple, nonZeroEdge),  #type: ignore
            np.stack(np.where(
                np.triu(self.__adjacencyMatrix) == -1
            )).T
        ))
        self.__rng.shuffle(order)
        self.__order: Iterator[tuple[int, int]] = iter(order)
        # Although the following quantities are computed by an adjacency matrix, we record them for a performance.
        self.__numOccupiedBonds: int = 0
        self.__numClusters: npt.NDArray[np.int_] = np.zeros(numNodes + 1, dtype=np.int_)
        self.__numClusters[1] = numNodes
        self.__firstClusterMoment: int = numNodes
        self.__secondClusterMoment: int = numNodes

    @property
    def AdjacencyMatrix(self) -> nptyping.NDArray[nptyping.Shape['Size, 2'], nptyping.Int]:
        return self.__adjacencyMatrix.copy()

    @property
    def NodeIndices(self) -> dict[Node, int]:
        return self.__nodeIndices

    @property
    def NumClusters(self) -> npt.NDArray[np.int_]:
        return self.__numClusters.copy()

    @property
    def NumOccupiedBonds(self) -> int:
        return self.__numOccupiedBonds

    @property
    def FirstClusterMoment(self) -> int:
        return self.__firstClusterMoment

    @property
    def SecondClusterMoment(self) -> int:
        return self.__secondClusterMoment

    def DoesExistConnectionBetween(self, node1: Node, node2: Node) -> bool:
        return self.__findRoot(self.__nodeIndices[node1]) == self.__findRoot(self.__nodeIndices[node2])

    def GetClusterSizeContaining(self, node: Node) -> int:
        return -self.__parent[self.__findRoot(self.__nodeIndices[node])]

    #def GetNodesBelongingToMaximumCluster(self) -> list[Node]:
    def GetIndicesBelongingToMaximumCluster(self) -> npt.NDArray[np.int_]:
        '''Generate a list of indices of nodes belonging to one of the maximum cluster

        Notes
        -----
        When there are some of the largest cluster, choose one of the clusters uniformly at random.
        '''
        maximumClusterIntex: int = self.__rng.choice(np.where(self.__parent == self.__parent.min())[0])
        indices: npt.NDArray[np.int_] = np.array(list(self.__nodeIndices.values()))
        indicesBelongingToMaximumCluster = indices[
            np.vectorize(
                lambda index: self.__findRoot(index) == maximumClusterIntex
            )(indices)
        ]
        #return [
        #    key
        #    for key, value in self.__nodeIndices.items()
        #    if value in indicesBelongingToMaximumCluster
        #]
        return indicesBelongingToMaximumCluster

    def SetSeed(self, seed: Optional[int]=None) -> None:
        self.__rng = np.random.default_rng(seed)

    def Update(self) -> None:
        try:
            newEdge: tuple[int, int] = next(self.__order)
            self.__adjacencyMatrix[newEdge[0], newEdge[1]] = self.__adjacencyMatrix[newEdge[1], newEdge[0]] = 1
            self.__numOccupiedBonds += 1
            self.__mergeRoots(self.__findRoot(newEdge[0]), self.__findRoot(newEdge[1]))
        except StopIteration:
            pass

    def Write(self) -> None:
        print('Adjacency matrix:')
        print(self.__adjacencyMatrix)
        print('Parent:')
        print(self.__parent)
        print('Cluster size distribution:')
        print(self.__numClusters)

    def __findRoot(self, node: int) -> int:
        '''Finding the root of `node`

        Parameters
        ----------
        node : int
            A node index looked for its root.

        Returns
        -------
        int
            The index of the root of `node`.

        Notes
        -----
        It must be guaranteed that each cluster contains only one root, whose parent is a negative integer.
        '''
        if self.__parent[node] < 0:
            return node
        else:
            self.__parent[node] = self.__findRoot(self.__parent[node])
            return self.__parent[node]

    def __mergeRoots(self, r1: int, r2: int) -> int:
        '''Merging two clusters containing `r1` and `r2`

        Parameters
        ----------
        r1 : int
            The index of a root.  Assume that its parent is a negative integer.
        r2 : int
            The index of a root.  Assume that its parent is a negative integer.

        Returns
        -------
        int
            The new index of the root of the merged cluster.
        '''
        if r1 == r2:
            return r1
        elif -self.__parent[r1] < -self.__parent[r2]:
            return self.__mergeRoots(r2, r1)
        else:
            # Update physical quantities.
            self.__numClusters[-self.__parent[r1]] -= 1
            self.__numClusters[-self.__parent[r2]] -= 1
            self.__numClusters[-self.__parent[r1] - self.__parent[r2]] += 1
            self.__firstClusterMoment += \
                -(self.__parent[r1] + self.__parent[r2]) \
                + self.__parent[r1] \
                + self.__parent[r2]
            self.__secondClusterMoment += \
                (self.__parent[r1] + self.__parent[r2]) ** 2 \
                - self.__parent[r1] ** 2 \
                - self.__parent[r2] ** 2
            # Merge two clusters.
            self.__parent[r1] += self.__parent[r2]
            self.__parent[r2] = r1
            return r1
