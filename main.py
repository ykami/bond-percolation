#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'Yoshinori Kamijima'
__date__ = '2022/9/24'

from collections.abc import Callable
import math
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import numpy.typing as npt
import pandas as pd
import simulator
import seaborn as sns
import utils

SideLength = 32
NumDivision = 10

def initialize(sideLength: int) -> tuple[nx.Graph, Callable[[simulator.BondPercolation], tuple[float, float]]]:
    graph: nx.Graph = nx.grid_2d_graph(SideLength, SideLength)
    mapping: dict[tuple[int, int], int] = {key: value for key, value in zip(graph.nodes, range(len(graph.nodes)))}
    graph = nx.relabel_nodes(graph, mapping)
    leftSideList = [value for key, value in mapping.items() if key[0] == 0]
    rightSideList = [value for key, value in mapping.items() if key[0] == SideLength - 1]
    leftSide, rightSide = np.meshgrid(leftSideList, rightSideList)

    def helperMethod(bondPercolation: simulator.BondPercolation) -> tuple[float, float]:
        spanningClusterSize = 0
        connectedIndices: npt.NDArray[np.bool_] = np.vectorize(bondPercolation.DoesExistConnectionBetween)(leftSide, rightSide)
        if np.any(connectedIndices):
            spanningClusterSize = bondPercolation.GetClusterSizeContaining(leftSide[connectedIndices][0])
        percolationProbability = spanningClusterSize / bondPercolation.FirstClusterMoment
        correctedSecondMoment = bondPercolation.SecondClusterMoment - spanningClusterSize ** 2
        correctedFirstMoment = bondPercolation.FirstClusterMoment - spanningClusterSize
        meanClusterSize = correctedSecondMoment / correctedFirstMoment if correctedFirstMoment > 0 else 1.e0
        return percolationProbability, meanClusterSize

    return graph, helperMethod

if __name__ == '__main__':
    # Initialize.
    sns.set()
    graph, calcPercolationProbabilityAndMeanClusterSize = initialize(SideLength)
    bp = utils.MakeFromNetworkxGraph(graph)
    numBonds = np.count_nonzero(np.triu(bp.AdjacencyMatrix) != 0)
    data = np.empty((numBonds + 1, 3), dtype=np.float_)
    nrows = math.floor(math.sqrt(NumDivision))
    ncols = math.ceil(NumDivision / nrows)
    pos = {node: (node % SideLength, node // SideLength) for node in graph.nodes}

    # Draw connected components and sample data of the physical quantities.
    fig, ax = plt.subplots(nrows, ncols, figsize=(ncols * 3, nrows * 3), tight_layout=True)
    ax = ax.ravel()
    for i in range(numBonds + 1):
        percolationProbability, meanClusterSize = calcPercolationProbabilityAndMeanClusterSize(bp)
        data[i, :] = [bp.NumOccupiedBonds / numBonds, percolationProbability, meanClusterSize]
        if i % (numBonds // NumDivision) == 0:
            adjacencyMatrix = bp.AdjacencyMatrix
            G = nx.from_numpy_array(np.where(adjacencyMatrix > 0, adjacencyMatrix, 0))
            indicesBelongingToMaximumCluster = bp.GetIndicesBelongingToMaximumCluster()
            nodeColor = [
                'r' if node in indicesBelongingToMaximumCluster else 'b'
                for node in G.nodes
            ]
            nx.draw_networkx(
                G,
                pos=pos,
                with_labels=False,
                node_size=4,
                node_color=nodeColor,
                ax=ax[i // (numBonds // NumDivision)]
            )
            ax[i // (numBonds // NumDivision)].set_axis_off()
        bp.Update()
    plt.show()

    # Plot graphs of the physical quantities with respect to the occupation probability.
    fig, ax = plt.subplots(2, 1, figsize=(6, 8), sharex=True, tight_layout=True)
    df = pd.DataFrame(
        data,
        index=range(numBonds + 1),
        columns=['occupation probability', 'percolation probability', 'mean cluster size']
    )
    sns.lineplot(data=df, x='occupation probability', y='percolation probability', ax=ax[0])
    sns.lineplot(data=df, x='occupation probability', y='mean cluster size', ax=ax[1])
    plt.show()
