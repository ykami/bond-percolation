# -*- coding: utf-8 -*-

from __future__ import annotations

__author__ = 'Yoshinori Kamijima'
__date__ = '2022/9/24'

import networkx as nx
import numpy as np
#from . import simulator
import simulator
from typing import Optional

NodeKeys = set[simulator.Node]

def MakeFromNetworkxGraph(graph: nx.Graph) -> simulator.BondPercolation:
    #relabeledGraph = nx.convert_node_labels_to_integers(graph)
    #return simulator.BondPercolation(set(relabeledGraph.edges))
    return simulator.BondPercolation(set(graph.edges))
